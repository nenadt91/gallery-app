package com.typeqast.data.remote.api

import com.typeqast.data.remote.models.PhotoAPI
import com.typeqast.data.remote.models.Wrapper
import retrofit2.http.GET
import retrofit2.http.Path

interface GalleryAPI {
    @GET("d397ad51-3e05-44a3-a1f2-c30ef9bef4ac")
    suspend fun getAllAlbums(): Wrapper

    @GET("{image}")
    suspend fun getImage(@Path("image") imageUri: String): PhotoAPI
}