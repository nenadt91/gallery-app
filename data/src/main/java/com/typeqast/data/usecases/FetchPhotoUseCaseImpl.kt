package com.typeqast.data.usecases

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.domain.repositores.GalleryRepository
import com.typeqast.domain.usecases.FetchPhotoUseCase
import com.typeqast.domain.wrappers.Resource

class FetchPhotoUseCaseImpl(
    private val galleryRepository: GalleryRepository,
) : FetchPhotoUseCase {
    override suspend fun execute(
        hasInternetConnection: Boolean,
        imageUrl: String
    ): LiveData<Resource<PhotoDOM>> {
        return liveData {
            try {
                emit(Resource.loading<List<PhotoDOM>>())
                if (!hasInternetConnection) {
                    emit(Resource.noInternet(null, null))
                    return@liveData
                }
                emit(Resource.success(galleryRepository.fetchImage(imageUrl), null))
            } catch (error: Error) {
                emit(Resource.error(null, error))
            }
        }
    }
}