package com.typeqast.data.dto

import com.typeqast.data.remote.models.Album
import com.typeqast.data.remote.models.GalleryEntityAPI
import com.typeqast.domain.mappers.DomainMapping
import com.typeqast.domain.models.AlbumDOM
import com.typeqast.domain.models.GalleryEntityDOM
import java.util.function.Consumer

class GalleryDTO : DomainMapping<List<GalleryEntityDOM>, List<GalleryEntityAPI>> {
    override fun mapToDomain(remote: List<GalleryEntityAPI>): List<GalleryEntityDOM> {
        val domainImages = ArrayList<GalleryEntityDOM>()
        remote.forEach(Consumer {
            domainImages.add(
                GalleryEntityDOM(
                    it.id,
                    it.name,
                    it.coverImage,
                    mapAlbum(it.album)
                )
            )
        })
        return domainImages.toList()
    }

    private fun mapAlbum(album: List<Album>): List<AlbumDOM> {
        val albumDOM = ArrayList<AlbumDOM>()
        album.forEach {
            albumDOM.add(
                AlbumDOM(
                    it.name,
                    it.thumbnailImage,
                    it.imageUrlPath
                )
            )
        }
        return albumDOM.toList()
    }

}