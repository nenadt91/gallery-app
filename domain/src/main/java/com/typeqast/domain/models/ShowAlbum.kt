package com.typeqast.domain.models

data class ShowAlbum(val id: Int, val name: String?, val url: String?)
