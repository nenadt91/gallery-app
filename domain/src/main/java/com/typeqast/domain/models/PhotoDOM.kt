package com.typeqast.domain.models

data class PhotoDOM(
    val name: String,
    val thumbnailUrl: String,
    val photoUrl: String
)
