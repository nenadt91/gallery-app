package com.typeqast.domain.models

data class AlbumDOM(
    val name: String,
    val thumbnailImage: String,
    val imageUrlPath: String
)
