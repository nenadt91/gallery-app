package com.typeqast.domain.models

data class ShowPhoto(val id: String?, val name: String?, val url: String?)