package com.typeqast.galleryapp.homescreen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.typeqast.core.ui.ICoreView
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.ShowAlbum
import com.typeqast.domain.wrappers.NetworkPolicy
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.ActicityHomeScreenBinding
import com.typeqast.galleryapp.homescreen.HomeScreenViewModel.Events.OPEN_PREVIEW_ALBUM_SCREEN
import com.typeqast.galleryapp.previewalbum.PreviewAlbumActivity
import com.typeqast.galleryapp.utils.ResponseWrapper
import com.typeqast.galleryapp.utils.ui.BaseActivity


class HomeScreenActivity : BaseActivity<ActicityHomeScreenBinding, HomeScreenViewModel>(),
    SwipeRefreshLayout.OnRefreshListener, AlbumAdapter.OnClickListener, View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUiComponents()
        loadAlbums()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun provideTAG(): String {
        return HomeScreenActivity::class.java.simpleName
    }

    override fun provideViewModel(): HomeScreenViewModel {
        return ViewModelProvider(this)[HomeScreenViewModel::class.java]
    }

    override fun provideLayout(): Int {
        return R.layout.acticity_home_screen
    }

    override fun onRefresh() {
        if (getBinding().srLayout.isRefreshing) {
            getViewModel()
                .dispatchEvent(HomeScreenViewModel.Events.FETCH_ALBUMS, null)
        }
    }

    private fun setupUiComponents() {
        getBinding()
            .rvGallery
            .apply {
                adapter = AlbumAdapter(this@HomeScreenActivity)
                layoutManager = LinearLayoutManager(
                    context, RecyclerView.HORIZONTAL, false
                )
            }

        getBinding()
            .srLayout
            .setOnRefreshListener(this)

        getBinding()
            .card
            .setOnClickListener(this)
    }

    private fun loadAlbums() {
        getViewModel()
            .fetchGalleryAlbums()
            .observe(
                this,
                object : ResponseWrapper<List<GalleryEntityDOM>, ICoreView>(this) {
                    override fun updateUI(status: Int) {
                        when (status) {
                            NetworkPolicy.Status.LOADING -> {
                                getBinding().pbLoading.visibility = View.VISIBLE
                            }
                            else -> {
                                getBinding().srLayout.isRefreshing = false
                                getBinding().pbLoading.visibility = View.GONE
                            }
                        }
                    }

                    override fun onSuccess(data: List<GalleryEntityDOM>?) {
                        val albumAdapter = getBinding().rvGallery.adapter as AlbumAdapter
                        albumAdapter.setItems(data)
                        if (albumAdapter.itemCount > 0) {
                            ((getBinding().rvGallery.layoutManager) as LinearLayoutManager)
                                .scrollToPosition(
                                    0
                                )
                            onItemClicked(data?.get(0))
                        }
                    }
                }
            )

        getViewModel()
            .navigateToSelectedAlbum()
            .observe(this, {
                val albumId = it.first.id
                val intent = Intent(this, PreviewAlbumActivity::class.java)
                intent.putExtra(EXTRA_ALBUM_ID, albumId)
                if (albumId > 0) {
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        this, R.string.error_album_id,
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }

    override fun onItemClicked(item: GalleryEntityDOM?) {
        getViewModel().dispatchEvent(
            HomeScreenViewModel.Events.SET_ALBUM,
            ShowAlbum(
                item?.id ?: 0,
                item?.name,
                item?.coverImage
            )
        )
    }

    override fun onClick(view: View?) {
        getViewModel().dispatchEvent(OPEN_PREVIEW_ALBUM_SCREEN, null)
    }

    companion object Constants {
        const val EXTRA_ALBUM_ID = "EXTRA_ALBUM_ID"
    }
}