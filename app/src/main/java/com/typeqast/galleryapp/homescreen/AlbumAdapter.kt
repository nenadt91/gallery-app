package com.typeqast.galleryapp.homescreen

import android.view.animation.AnimationUtils
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.ItemAlbumOverviewBinding

import com.typeqast.galleryapp.utils.adapters.BaseBindingAdapter
import com.typeqast.galleryapp.utils.adapters.BaseViewHolder

class AlbumAdapter(private val listener: OnClickListener) : BaseBindingAdapter<GalleryEntityDOM>() {

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_album_overview
    }

    override fun onBindViewHolder(holder: BaseViewHolder<GalleryEntityDOM>, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder.getBinding() as ItemAlbumOverviewBinding).root.setOnClickListener {
            this.listener.onItemClicked(
                getItem(holder.bindingAdapterPosition) as GalleryEntityDOM
            )
        }
        (holder.getBinding() as ItemAlbumOverviewBinding).root.animation =
            AnimationUtils.loadAnimation(holder.itemView.context, R.anim.animation_list)
    }

    interface OnClickListener {
        fun onItemClicked(item: GalleryEntityDOM?)
    }
}