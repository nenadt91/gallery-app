package com.typeqast.galleryapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.typeqast.galleryapp.R

object BindingUtils {
    @JvmStatic
    @BindingAdapter("imgUrl")
    fun loadImage(imageView: ImageView, url: String?) {
        Glide
            .with(imageView.context)
            .load(url)
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_loading)
            .fitCenter()
            .into(imageView)
    }
}