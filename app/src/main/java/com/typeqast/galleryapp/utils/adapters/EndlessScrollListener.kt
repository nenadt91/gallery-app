package com.typeqast.galleryapp.utils.adapters

import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessScrollListener(private val mLinearLayoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    private var previousTotal = 0 // The total number of items in the data-set after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private var visibleThreshold =
        3 // The minimum amount of items to have below your current // scroll position before loading more.

    private var currentPage = 1

    private var enabled: Boolean = false

    fun setVisibleThreshold(visibleThreshold: Int) {
        this.visibleThreshold = visibleThreshold
    }

    internal fun setEnabled(enabled: Boolean) {
        this.enabled = enabled
    }

    internal fun getEnabled(): Boolean {
        return enabled
    }

    override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (!enabled) {
            return
        }

        val visibleItemCount = recyclerView.childCount
        val totalItemCount = mLinearLayoutManager.itemCount
        val firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // End has been reached

            currentPage++

            onLoadMore(currentPage)

            loading = true
        }
    }

    internal abstract fun onLoadMore(currentPage: Int)

    fun reset() {
        currentPage = 0
        previousTotal = 0
    }

}

