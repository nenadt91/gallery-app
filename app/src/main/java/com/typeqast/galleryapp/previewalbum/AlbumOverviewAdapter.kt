package com.typeqast.galleryapp.previewalbum

import android.view.animation.AnimationUtils
import com.typeqast.domain.models.AlbumDOM
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.ItemPhotosOverviewBindingImpl
import com.typeqast.galleryapp.utils.adapters.BaseBindingAdapter
import com.typeqast.galleryapp.utils.adapters.BaseViewHolder

class AlbumOverviewAdapter(private val listener: OnClickListener) :
    BaseBindingAdapter<AlbumDOM>() {
    override fun getItemViewType(position: Int): Int {
        return R.layout.item_photos_overview
    }

    override fun onBindViewHolder(holder: BaseViewHolder<AlbumDOM>, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder.getBinding() as ItemPhotosOverviewBindingImpl).root.setOnClickListener {
            this.listener.onItemClicked(
                getItem(holder.bindingAdapterPosition) as AlbumDOM
            )
        }
        (holder.getBinding() as ItemPhotosOverviewBindingImpl).root.animation =
            AnimationUtils.loadAnimation(holder.itemView.context, R.anim.animation_list)
    }


    interface OnClickListener {
        fun onItemClicked(item: AlbumDOM?)
    }
}