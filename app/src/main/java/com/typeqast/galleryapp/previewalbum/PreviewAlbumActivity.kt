package com.typeqast.galleryapp.previewalbum

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.ActivityPreviewAlbumBinding
import com.typeqast.galleryapp.homescreen.HomeScreenActivity
import com.typeqast.galleryapp.utils.ui.BaseActivity

class PreviewAlbumActivity : BaseActivity<ActivityPreviewAlbumBinding, PreviewAlbumViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val albumId = intent.getIntExtra(
            HomeScreenActivity.EXTRA_ALBUM_ID,
            0
        )

        albumId.let {
            if (it < 1) {
                Toast.makeText(
                    this, R.string.error_album_id,
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        getViewModel()
            .dispatchEvent(Pair(PreviewAlbumViewModel.Events.STORE_ALBUM_ID, albumId))
        getViewModel()
            .dispatchEvent(Pair(PreviewAlbumViewModel.Events.NAVIGATE_TO_PREVIEW_FRAGMENT, null))

        getViewModel()
            .observeViewActions()
            .observe(this,
                { dispatchViewActions(it.first, it.second) }
            )
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
            return
        }
        finish()
    }


    override fun provideTAG(): String {
        return PreviewAlbumActivity::class.java.name
    }

    override fun provideViewModel(): PreviewAlbumViewModel {
        return ViewModelProvider(this)[PreviewAlbumViewModel::class.java]
    }

    override fun provideLayout(): Int {
        return R.layout.activity_preview_album
    }

    private fun dispatchViewActions(actions: PreviewAlbumViewModel.Actions, data: Any?) {
        when (actions) {
            PreviewAlbumViewModel.Actions.NAVIGATE_TO_SHOW_PHOTO ->
                navigateToPhotoOverviewFragment()
            PreviewAlbumViewModel.Actions.NAVIGATE_TO_SHOW_ALBUM ->
                navigateToAlbumOverviewFragment()
        }
    }

    private fun navigateToPhotoOverviewFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, PreviewPhotoFragment(), null).addToBackStack(null)
            .commit()
    }

    private fun navigateToAlbumOverviewFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, AlbumOverviewFragment(), null).addToBackStack(null)
            .commit()
    }

}